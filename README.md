# 🥃 Bookowski

Any book, any time, anywhere.

Supported:
- Goodreads
- Amazon
- Kobo Books
- Wordery
- Blackwell's
- (Awesomebooks)

[![goodreads](https://i.postimg.cc/85vcDhXM/image.png)](https://postimg.cc/YGpMzmHC)

[![amazon](https://i.postimg.cc/c1QCbBnN/Clean-Shot-2024-08-17-at-00-48-55-2x.png)](https://postimg.cc/hhtKJxsC)

[![kobo](https://i.postimg.cc/MpGGXkrW/Clean-Shot-2024-08-17-at-00-49-40-2x.png)](https://postimg.cc/HVfmPNgK)

[![wordery](https://i.postimg.cc/cJr6kqQD/Clean-Shot-2024-08-17-at-00-46-54-2x.png)](https://postimg.cc/Y4BpjsDg)

[![blackwells](https://i.postimg.cc/9QNQ7xvy/Clean-Shot-2024-08-17-at-00-50-35-2x.png)](https://postimg.cc/xXMQrPjd)