import { retry } from 'radash'

export const matchHost = (host: string) => {
	if (typeof window !== 'undefined') {
		return window.location.hostname.includes(host)
	}
	return false
}

type ExtractStructuredDataOptions = {
	type?: string
}

export const extractStructuredData = async <T>(
	opts: ExtractStructuredDataOptions = {}
): Promise<T | {}> => {
	const { type } = Object.assign({ type: 'Book' }, opts)

	try {
		return await retry(
			{
				times: 10,
				delay: 150,
			},
			async (exit) => {
				const jsonLdScripts = [
					...document.querySelectorAll('script[type="application/ld+json"]'),
				]

				if (!jsonLdScripts.length) {
					console.warn('No JSON-LD scripts found')
					exit(new Error('No JSON-LD scripts found')) // Exit on this fatal error
					return {} as T
				}

				const jsonLdContents = jsonLdScripts
					.map((script) => script.textContent)
					.filter(Boolean) as string[]

				const jsonLds = jsonLdContents.map((s) => JSON.parse(s))
				const entry = jsonLds.find((jsonLd) => jsonLd['@type'] === type)

				if (entry) {
					return entry as T
				} else {
					// Do not call exit, allow retry to handle this case
					throw new Error('Entry not found') // Trigger retry
				}
			}
		)
	} catch (error) {
		// If all retries fail, return an empty object
		console.error('Error extracting structured data', error)
		return {} as T
	}
}

export const decodeHTMLEntities = (text: string) => {
	const tempElement = document.createElement('textarea')
	tempElement.innerHTML = text
	return tempElement.value
}

export const printAppTitle = () => {
	if (typeof window !== 'undefined') {
		console.log(
			'%c\nBOOKOWSKI 🥃',
			'color: #FFD846; font-size: 32px; font-weight: bold;'
		)
	} else {
		console.log('Bookowski')
	}
}
