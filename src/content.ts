import { hostWebsites, resultWebsites } from './config'
import { HostWebsiteConfig, ResWebsiteId, WebsiteSearchResult } from './types'
import { decodeHTMLEntities, matchHost, printAppTitle } from './utils'

const handleSearchByQuery = (
	search: (query: string) => string,
	query?: string
) => {
	if (!query) {
		return undefined
	}
	const normalizedQuery = decodeHTMLEntities(query)
	return search(normalizedQuery)
}

const main = async () => {
	printAppTitle()

	const entries = Object.values(hostWebsites) as HostWebsiteConfig[]

	let res: Partial<Record<ResWebsiteId, WebsiteSearchResult>> = {}

	for (const { host, getRawData, parseData } of entries) {
		const matched = matchHost(host)
		if (!matched) {
			continue
		}

		console.log(`🎯 Matched ${host}`)

		// Await the asynchronous getRawData call
		const rawData = await getRawData()

		// Parse the raw data synchronously
		const parsedData = parseData(rawData)

		const { isbn, title, author } = parsedData

		console.log(isbn ? `✅ ISBN: ${isbn}` : '❗ No ISBN found')
		console.log(title ? `✅ Title: ${title}` : '❗ No title found')
		console.log(author ? `✅ Author: ${author}` : '❗ No author found')

		const titleWithAuthor = title && author ? `${title} ${author}` : undefined

		if (!(isbn || title || titleWithAuthor)) continue

		// Reduce the resultWebsites and accumulate the result in the 'res' object
		Object.entries(resultWebsites).forEach(
			([resWebsite, { label: resWebsiteLabel, search }]) => {
				res[resWebsite as ResWebsiteId] = {
					label: resWebsiteLabel,
					url: {
						byIsbn: handleSearchByQuery(search, isbn),
						byTitle: handleSearchByQuery(search, title),
						byTitleWithAuthor: handleSearchByQuery(search, titleWithAuthor),
					},
				}
			}
		)
	}

	// Display the results
	Object.values(res).forEach(({ label, url }) => {
		console.group(`🔗 ${label}:`)
		console.log(`(isbn):\n${url.byIsbn ?? '❌'}`)
		console.log(`(title):\n${url.byTitle ?? '❌'}`)
		console.log(`(title+author):\n${url.byTitleWithAuthor ?? '❌'}`)
		console.groupEnd()
	})
}

main()
