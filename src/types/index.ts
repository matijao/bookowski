import { hostWebsites, resultWebsites } from '../config'

export type WebsiteSearchResult = {
	label: string
	url: {
		byIsbn?: string
		byTitle?: string
		byTitleWithAuthor?: string
	}
}

export type HostWebsiteId = keyof typeof hostWebsites
export type ResWebsiteId = keyof typeof resultWebsites

export type DownloadResMap = Record<HostWebsiteId, Record<ResWebsiteId, string>>

export type MaybePromise<T> = T | Promise<T>

export type BookData = {
	isbn?: string
	isbn10?: string
	title?: string
	author?: string
}

export type HostWebsiteKey =
	| 'goodreads'
	| 'kobo'
	| 'amazon'
	| 'wordery'
	| 'blackwells'
export type HostWebsiteConfig = {
	host: string
	getRawData: () => MaybePromise<any>
	parseData: (data: any) => BookData
}
