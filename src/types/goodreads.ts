interface Root {
	'@context': string
	'@type': string
	name: string
	image: string
	bookFormat: string
	numberOfPages: number
	inLanguage: string
	isbn: string
	awards: string
	author: Author[]
	aggregateRating: AggregateRating
}

interface AggregateRating {
	'@type': string
	ratingValue: number
	ratingCount: number
	reviewCount: number
}

interface Author {
	'@type': string
	name: string
	url: string
}

export type GoodreadsBookData = Partial<Root>
