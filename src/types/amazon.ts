interface RootObject {
	treatment: string
	eventPreviewTreatment: string
	shareDataAttributes: ShareDataAttributes
	isOGTEnabled: boolean
	aapiBaseUrl: string
	title: string
	refererURL: string
	emailSubject: string
	url: string
	dealsPreviewEnabled: boolean
	skipTwisterAPI: string
	isUnrecognizedUsersRichPreviewEnabled: boolean
	t: T
	weblab: string
	mailToUri: string
	refId: string
	shareAapiCsrfToken: string
	isIpadFixesEnabled: boolean
	tinyUrlEnabled: boolean
}

interface T {
	taf_twitter_name: string
	taf_copy_url_changeover: string
	taf_pinterest_name: string
	taf_share_bottom_sheet_title: string
	taf_copy_tooltip: string
	taf_email_tooltip: string
	taf_copy_name: string
	taf_email_name: string
	taf_facebook_name: string
	taf_twitter_tooltip: string
	taf_facebook_tooltip: string
	taf_pinterest_tooltip: string
}

interface ShareDataAttributes {
	isInternal: boolean
	marketplaceId: string
	ingress: string
	isRobot: boolean
	requestId: string
	customerId: string
	asin: string
	userAgent: string
	platform: string
}

export type AmazonBookData = Partial<RootObject>
