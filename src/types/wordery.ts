interface RootObject {
  '@context': string;
  '@type': string;
  name: string;
  image: string;
  isbn: string;
  description: string;
  publisher: Publisher;
  contributor: Contributor[][];
  bookEdition: string;
  keywords: string;
  datePublished: string;
  numberOfPages: string;
  offers: Offers;
}

interface Offers {
  '@type': string;
  price: string;
  priceCurrency: string;
  itemCondition: string;
  availability: string;
}

interface Contributor {
  '@type': string;
  name: string;
  url: string;
}

interface Publisher {
  url: string;
}

export type WorderyBookData = Partial<RootObject>
