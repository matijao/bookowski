interface RootObject {
	'@context': string
	'@type': string
	name: string
	genre: string[]
	inLanguage: string
	author: Author
	workExample: WorkExample
	url: string
	bookFormat: string
	aggregateRating: AggregateRating
	publisher: Author
	numberOfPages: number
	timeRequired: string
	isAccessibleForFree: boolean
	review: Review[]
}

interface Review {
	'@type': string
	reviewRating: ReviewRating
	name: string
	author: Author
	datePublished: string
	reviewBody: string
}

interface ReviewRating {
	'@type': string
	ratingValue: string
}

interface AggregateRating {
	'@type': string
	bestRating: string
	ratingValue: string
	reviewCount: string
	ratingCount: string
}

interface WorkExample {
	'@type': string
	author: Author
	isbn: string
	bookFormat: string
	potentialAction: PotentialAction
	datePublished: string
	description: string
	image: string
	thumbnailUrl: string
	alternativeHeadline: string
	isAccessibleForFree: boolean
}

interface PotentialAction {
	'@type': string
	target: Target
	expectsAcceptanceOf: ExpectsAcceptanceOf
}

interface ExpectsAcceptanceOf {
	'@type': string
	price: number
	priceCurrency: string
	eligibleRegion: Author[]
	ineligibleRegion: Author[]
	availability: string
}

interface Target {
	'@type': string
	urlTemplate: string
	actionPlatform: string[]
}

interface Author {
	'@type': string
	name: string
}

export type KoboBookData = Partial<RootObject>
