import { HostWebsiteConfig, HostWebsiteKey } from './types'
import { AmazonBookData } from './types/amazon'
import { GoodreadsBookData } from './types/goodreads'
import { KoboBookData } from './types/kobo'
import { WorderyBookData } from './types/wordery'
import { decodeHTMLEntities, extractStructuredData } from './utils'

export const hostWebsites: Record<HostWebsiteKey, HostWebsiteConfig> = {
	goodreads: {
		host: 'goodreads.com',
		getRawData: async () => {
			return await extractStructuredData<GoodreadsBookData>()
		},
		parseData: (data: GoodreadsBookData) => {
			return {
				isbn: data.isbn,
				title: data.name,
				author: data.author?.[0]?.name,
			}
		},
	},
	kobo: {
		host: 'kobo.com',
		getRawData: async () => {
			return await extractStructuredData<KoboBookData>()
		},
		parseData: (data: KoboBookData) => {
			return {
				isbn: data.workExample?.isbn,
				title: data.name,
				author: data.author?.name,
			}
		},
	},
	wordery: {
		host: 'wordery.com',
		getRawData: async () => {
			return await extractStructuredData<WorderyBookData>()
		},
		parseData: (data: WorderyBookData) => {
			return {
				isbn: data.isbn,
				title: data.name,
				author: data.contributor?.[0]?.[0]?.name,
			}
		},
	},
	blackwells: {
		host: 'blackwells.co.uk',
		getRawData: async () => {
			const isbn = document
				.querySelector('meta[property="book:isbn"]')
				?.getAttribute('content')

			const title = document
				.querySelector('meta[property="og:title"]')
				?.getAttribute('content')

			// Use querySelector to find the author element
			const author = document
				.querySelector('.product__author[itemprop="author"] a')
				?.textContent?.trim()

			return {
				isbn,
				title,
				author,
			}
		},
		parseData: (data: any) => {
			return {
				isbn: data.isbn,
				title: data.title,
				author: data.author,
			}
		},
	},
	amazon: {
		host: 'amazon.com',
		getRawData: async () => {
			const asin = document
				.querySelector('[data-asin]')
				?.getAttribute('data-asin')

			const author = document.querySelector('.author > a')?.textContent?.trim()

			const dataDump = document
				.querySelector('[data-ssf-share-icon]')
				?.getAttribute('data-ssf-share-icon')

			const decodedData = decodeHTMLEntities(dataDump || '{}')

			try {
				const data = JSON.parse(decodedData) as AmazonBookData
				return {
					isbn: asin ?? data.shareDataAttributes?.asin,
					data,
					author,
				}
			} catch (err) {
				return {
					isbn: asin,
				}
			}
		},
		parseData: (data: {
			isbn?: string
			author?: string
			data?: AmazonBookData
		}) => {
			return {
				isbn: data.isbn,
				author: data.author,
				title: data.data?.title,
			}
		},
	},
}

export const resultWebsites = {
	anna: {
		label: "Anna's Archive",
		search: (query: string) => {
			const baseUrl = 'https://annas-archive.org'
			const languages = ['en']
			const extensions = ['epub']

			const url = new URL(`${baseUrl}/search`)

			const params = new URLSearchParams(url.search)
			params.append('index', '')
			params.append('q', query)
			extensions.forEach((ext) => params.append('ext', ext))
			languages.forEach((lang) => params.append('lang', lang))

			url.search = params.toString()

			return url.href
		},
	},
	zlib: {
		label: 'Z-Library',
		search: (query: string) => {
			query = encodeURIComponent(query)

			const baseUrl = 'https://z-library.rs'
			const languages = ['english']
			const extensions = ['EPUB']

			const url = new URL(`${baseUrl}/s/${query}`)
			const params = new URLSearchParams(url.search)

			languages.forEach((lang, idx) => params.append(`languages[${idx}]`, lang))
			extensions.forEach((ext, idx) => params.append(`extensions[${idx}]`, ext))

			url.search = params.toString()

			return url.href
		},
	},
	libgen: {
		label: 'LibGen',
		search: (query: string) => {
			query = encodeURIComponent(query)

			return `https://libgen.is/search.php?req=${query}&open=0&res=25&view=simple&phrase=1`
		},
	},
}
