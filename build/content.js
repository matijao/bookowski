// node_modules/.pnpm/radash@12.1.0/node_modules/radash/dist/esm/typed.mjs
var isArray = Array.isArray;
var isFunction = (value) => {
  return !!(value && value.constructor && value.call && value.apply);
};
var isPromise = (value) => {
  if (!value)
    return false;
  if (!value.then)
    return false;
  if (!isFunction(value.then))
    return false;
  return true;
};

// node_modules/.pnpm/radash@12.1.0/node_modules/radash/dist/esm/array.mjs
function* range(startOrLength, end, valueOrMapper = (i) => i, step = 1) {
  const mapper = isFunction(valueOrMapper) ? valueOrMapper : () => valueOrMapper;
  const start = end ? startOrLength : 0;
  const final = end ?? startOrLength;
  for (let i = start;i <= final; i += step) {
    yield mapper(i);
    if (i + step > final)
      break;
  }
}

// node_modules/.pnpm/radash@12.1.0/node_modules/radash/dist/esm/async.mjs
var retry = async (options, func) => {
  const times = options?.times ?? 3;
  const delay = options?.delay;
  const backoff = options?.backoff ?? null;
  for (const i of range(1, times)) {
    const [err, result] = await tryit(func)((err2) => {
      throw { _exited: err2 };
    });
    if (!err)
      return result;
    if (err._exited)
      throw err._exited;
    if (i === times)
      throw err;
    if (delay)
      await sleep(delay);
    if (backoff)
      await sleep(backoff(i));
  }
  return;
};
var sleep = (milliseconds) => {
  return new Promise((res) => setTimeout(res, milliseconds));
};
var tryit = (func) => {
  return (...args) => {
    try {
      const result = func(...args);
      if (isPromise(result)) {
        return result.then((value) => [undefined, value]).catch((err) => [err, undefined]);
      }
      return [undefined, result];
    } catch (err) {
      return [err, undefined];
    }
  };
};
// src/utils/index.ts
var matchHost = (host) => {
  if (typeof window !== "undefined") {
    return window.location.hostname.includes(host);
  }
  return false;
};
var extractStructuredData = async (opts = {}) => {
  const { type } = Object.assign({ type: "Book" }, opts);
  try {
    return await retry({
      times: 10,
      delay: 150
    }, async (exit) => {
      const jsonLdScripts = [
        ...document.querySelectorAll('script[type="application/ld+json"]')
      ];
      if (!jsonLdScripts.length) {
        console.warn("No JSON-LD scripts found");
        exit(new Error("No JSON-LD scripts found"));
        return {};
      }
      const jsonLdContents = jsonLdScripts.map((script) => script.textContent).filter(Boolean);
      const jsonLds = jsonLdContents.map((s) => JSON.parse(s));
      const entry = jsonLds.find((jsonLd) => jsonLd["@type"] === type);
      if (entry) {
        return entry;
      } else {
        throw new Error("Entry not found");
      }
    });
  } catch (error) {
    console.error("Error extracting structured data", error);
    return {};
  }
};
var decodeHTMLEntities = (text) => {
  const tempElement = document.createElement("textarea");
  tempElement.innerHTML = text;
  return tempElement.value;
};
var printAppTitle = () => {
  if (typeof window !== "undefined") {
    console.log(`%c
BOOKOWSKI \uD83E\uDD43`, "color: #FFD846; font-size: 32px; font-weight: bold;");
  } else {
    console.log("Bookowski");
  }
};

// src/config.ts
var hostWebsites = {
  goodreads: {
    host: "goodreads.com",
    getRawData: async () => {
      return await extractStructuredData();
    },
    parseData: (data) => {
      return {
        isbn: data.isbn,
        title: data.name,
        author: data.author?.[0]?.name
      };
    }
  },
  kobo: {
    host: "kobo.com",
    getRawData: async () => {
      return await extractStructuredData();
    },
    parseData: (data) => {
      return {
        isbn: data.workExample?.isbn,
        title: data.name,
        author: data.author?.name
      };
    }
  },
  wordery: {
    host: "wordery.com",
    getRawData: async () => {
      return await extractStructuredData();
    },
    parseData: (data) => {
      return {
        isbn: data.isbn,
        title: data.name,
        author: data.contributor?.[0]?.[0]?.name
      };
    }
  },
  blackwells: {
    host: "blackwells.co.uk",
    getRawData: async () => {
      const isbn = document.querySelector('meta[property="book:isbn"]')?.getAttribute("content");
      const title = document.querySelector('meta[property="og:title"]')?.getAttribute("content");
      const author = document.querySelector('.product__author[itemprop="author"] a')?.textContent?.trim();
      return {
        isbn,
        title,
        author
      };
    },
    parseData: (data) => {
      return {
        isbn: data.isbn,
        title: data.title,
        author: data.author
      };
    }
  },
  amazon: {
    host: "amazon.com",
    getRawData: async () => {
      const asin = document.querySelector("[data-asin]")?.getAttribute("data-asin");
      const author = document.querySelector(".author > a")?.textContent?.trim();
      const dataDump = document.querySelector("[data-ssf-share-icon]")?.getAttribute("data-ssf-share-icon");
      const decodedData = decodeHTMLEntities(dataDump || "{}");
      try {
        const data = JSON.parse(decodedData);
        return {
          isbn: asin ?? data.shareDataAttributes?.asin,
          data,
          author
        };
      } catch (err) {
        return {
          isbn: asin
        };
      }
    },
    parseData: (data) => {
      return {
        isbn: data.isbn,
        author: data.author,
        title: data.data?.title
      };
    }
  }
};
var resultWebsites = {
  anna: {
    label: "Anna's Archive",
    search: (query) => {
      const baseUrl = "https://annas-archive.org";
      const languages = ["en"];
      const extensions = ["epub"];
      const url = new URL(`${baseUrl}/search`);
      const params = new URLSearchParams(url.search);
      params.append("index", "");
      params.append("q", query);
      extensions.forEach((ext) => params.append("ext", ext));
      languages.forEach((lang) => params.append("lang", lang));
      url.search = params.toString();
      return url.href;
    }
  },
  zlib: {
    label: "Z-Library",
    search: (query) => {
      query = encodeURIComponent(query);
      const baseUrl = "https://z-library.rs";
      const languages = ["english"];
      const extensions = ["EPUB"];
      const url = new URL(`${baseUrl}/s/${query}`);
      const params = new URLSearchParams(url.search);
      languages.forEach((lang, idx) => params.append(`languages[${idx}]`, lang));
      extensions.forEach((ext, idx) => params.append(`extensions[${idx}]`, ext));
      url.search = params.toString();
      return url.href;
    }
  },
  libgen: {
    label: "LibGen",
    search: (query) => {
      query = encodeURIComponent(query);
      return `https://libgen.is/search.php?req=${query}&open=0&res=25&view=simple&phrase=1`;
    }
  }
};

// src/content.ts
var handleSearchByQuery = (search, query) => {
  if (!query) {
    return;
  }
  const normalizedQuery = decodeHTMLEntities(query);
  return search(normalizedQuery);
};
var main = async () => {
  printAppTitle();
  const entries = Object.values(hostWebsites);
  let res = {};
  for (const { host, getRawData, parseData } of entries) {
    const matched = matchHost(host);
    if (!matched) {
      continue;
    }
    console.log(`\uD83C\uDFAF Matched ${host}`);
    const rawData = await getRawData();
    const parsedData = parseData(rawData);
    const { isbn, title, author } = parsedData;
    console.log(isbn ? `\u2705 ISBN: ${isbn}` : "\u2757 No ISBN found");
    console.log(title ? `\u2705 Title: ${title}` : "\u2757 No title found");
    console.log(author ? `\u2705 Author: ${author}` : "\u2757 No author found");
    const titleWithAuthor = title && author ? `${title} ${author}` : undefined;
    if (!(isbn || title || titleWithAuthor))
      continue;
    Object.entries(resultWebsites).forEach(([resWebsite, { label: resWebsiteLabel, search }]) => {
      res[resWebsite] = {
        label: resWebsiteLabel,
        url: {
          byIsbn: handleSearchByQuery(search, isbn),
          byTitle: handleSearchByQuery(search, title),
          byTitleWithAuthor: handleSearchByQuery(search, titleWithAuthor)
        }
      };
    });
  }
  Object.values(res).forEach(({ label, url }) => {
    console.group(`\uD83D\uDD17 ${label}:`);
    console.log(`(isbn):\n${url.byIsbn ?? "\u274C"}`);
    console.log(`(title):\n${url.byTitle ?? "\u274C"}`);
    console.log(`(title+author):\n${url.byTitleWithAuthor ?? "\u274C"}`);
    console.groupEnd();
  });
};
main();
